/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <string>
#include <cstring>
#include <iostream>
#include <fstream>
#include "Cache.h"

using namespace std;

const size_t BUFFER_SIZE = 1024;
const size_t CACHE_SIZE = 64 * 1024 * 1024;

void error(const char *msg) {
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[]) {
    int sockfd, newsockfd, port;
    char buffer[BUFFER_SIZE];
    struct sockaddr_in serv_addr, cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int n;

    /* check arguments */
    if (argc != 3) {
        fprintf(stderr, "Usage : %s port_to_listen_on file_directory\n", argv[0]);
        exit(-1);
    }

    Cache server_cache(CACHE_SIZE); //64MB cache

    string filePath = string(argv[2]);
    /* check if the file path exist */
    struct stat sb;
    if(stat(argv[2], &sb) == -1) {
        error("stat error");
    }

    /* create socket
     * the 2nd parameter is type of socket. SOCK_STREAM is TCP
     * the 3rd parameter is protocol. Set as 0 if the let the OS choose automatically
     * */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR opening socket");
    }

    /* check port number */
    port = atoi(argv[1]);
    if (port < 2000 || port > 65535) {
        error("ERROR on port number");
    }

    /* set serv_addr */
    bzero((char *) &serv_addr, sizeof(serv_addr));
    /* set domain of server address as Internet domain */
    serv_addr.sin_family = AF_INET;
    /* set server ip address as current machine's address */
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    /* convert port number to network encoding*/
    serv_addr.sin_port = htons(port);

    /* bind server address to socket sockfd */
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        /* The most common failure is that this socket is already in use on this machine */
        error("ERROR on binding");
    }

    /* listen on the socket for connection
    * the 2nd parameter is backlog, which defines max number of connections can be waiting
    * while the process is handling current connection
    */
    listen(sockfd, 5);

    /****************** New Client Connection Request Comes ********************/
    while (true) {
        // show current cache
//        printf("before printAllFileNames\n");
//        server_cache.printAllFileNames();
//        printf("print all file names in cache complete\n");

        /* accept blocks the server process
        * new incoming connetion wakes up the process
        * accept returns newsockfd, further reading and writing would use this newsockfd rather than sockfd
        */
        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0) {
            error("ERROR on accept");
        }

        //printf("after accept\n");

        bzero(buffer, BUFFER_SIZE);
        /* read filename
        * no more than BUFFER_SIZE-1 bytes
        */
        n = read(newsockfd, buffer, BUFFER_SIZE - 1);
        if (n < 0) {
            //printf("buffer = %s\n", buffer);
            error("ERROR reading from socket");
        }

        //printf("read file name complete\n");

        // convert fileName in buffer to string
        string fileName = string(buffer);
        printf("Client %d.%d.%d.%d is requesting file %s\n",
                int(cli_addr.sin_addr.s_addr & 0xFF),
                int((cli_addr.sin_addr.s_addr & 0xFF00) >> 8),
                int((cli_addr.sin_addr.s_addr & 0xFF0000) >> 16),
                int((cli_addr.sin_addr.s_addr & 0xFF000000) >> 24),
                fileName.c_str()
        );

        /* Check file Existence */
        ifstream fileInputStream;
        string fullFileName = filePath + '/' + fileName;
        fileInputStream.open(fullFileName.c_str(), ios::in | ios::binary);
        if (!fileInputStream) {
            printf("File %s does not exist\n", fileName.c_str());
            // send state flag 0
            buffer[0] = '0';
            n = write(newsockfd, buffer, 1);
            if (n < 0) {
                error("ERROR writing msg to socket");
            }
        } else {
            // send state flag 1
            buffer[0] = '1';
            n = write(newsockfd, buffer, 1);
            if (n < 0) {
                error("ERROR writing msg to socket");
            }

            /* check cache */
            CacheNodePtr nodePtr = server_cache.get(fileName);
            if (nodePtr != NULL) {
                /***************** File is contained in cache *****************/
                // print cache hit msg
                printf("Cache hit. %s sent to client\n", fileName.c_str());
                /* send from memory to socket */
                //printf("DEBUG: cache hit: %x\n", (unsigned long long)(void*)nodePtr);
                size_t len = nodePtr->m_size;
                //printf("DEBUG: cache hit: strlen = %lu, len = %lu\n", strlen(nodePtr->m_content), len);
                size_t start = 0;
                while (start < len) {
                    //printf("DEBUG: cache hit: start = %lu\n", start);
                    bzero(buffer, BUFFER_SIZE);
                    size_t cp_len = len - start > BUFFER_SIZE ? BUFFER_SIZE : len - start;
                    //printf("DEBUG: cache hit : %lu\n", cp_len);
                    memcpy(buffer, nodePtr->m_content + start, cp_len);
                    n = write(newsockfd, buffer, cp_len);
                    if (n < 0) {
                        error("ERROR writing file to socket");
                    }
                    start += cp_len;
                }
            } else {
                /**************** File is not contained in cache ****************/
                // print cache miss msg
                printf("Cache miss. %s sent to client\n", fileName.c_str());
                /* read from disk to memory and send to socket at the same time */
                fileInputStream.seekg(0, fileInputStream.end);
                size_t fileSize = fileInputStream.tellg();
                //printf("fileSize = %lu\n", fileSize);
                fileInputStream.seekg(0, fileInputStream.beg);
                char *fileContent = new char[fileSize];
                size_t start = 0;

                //printf("DEBUG: after open file. fileSize = %lu\n", fileSize);

                while (!fileInputStream.eof()) {
                    bzero(buffer, BUFFER_SIZE);
                    fileInputStream.read(buffer, BUFFER_SIZE);
                    if (fileInputStream.bad()) {
                        error("ERROR read file from disk");
                    }
                    size_t read_size = fileInputStream.gcount();
                    //copy file to memory
                    memcpy(&fileContent[start], buffer, read_size);
                    start += read_size;
                    //send file to socket
                    n = write(newsockfd, buffer, read_size);
                    if (n < 0) {
                        error("ERROR writing file to socket");
                    }
                }

                //printf("DEBUG: after send file. fileSize = %lu\n", start);

                //load file to cache
                server_cache.insert(fileName, fileContent, fileSize);
                //printf("DEBUG: after insert\n");
                fileInputStream.close();
            }
        }
        close(newsockfd);
    }
    close(sockfd);
    return 0;
}