#ifndef CACHENODE_H
#define CACHENODE_H

#include <string>

using namespace std;

typedef struct FileNode{
public:
    string m_fileName;
    char* m_content;
    size_t m_size; //size in bytes
    FileNode* prev;
    FileNode* next;

    FileNode(string fileName) : m_fileName(fileName) {
        m_size = 0;
        m_content = NULL;
        prev = next = NULL;
    }
    FileNode(string fileName, char* content, size_t size) : m_fileName(fileName), m_content(content), m_size(size) {
        prev = next = NULL;
    }
//    ~FileNode() {
//        delete m_content;
//        delete prev;
//        delete next;
//    }
}CacheNode, *CacheNodePtr;

#endif
