#ifndef CACHE_H
#define CACHE_H

#include <unordered_map>
#include <string>
#include "CacheNode.cpp"

using namespace std;

class Cache{
private:
    unordered_map<string, CacheNodePtr> m_map;
    size_t m_capacity;
    size_t m_remainCapacity;
    CacheNodePtr head;
    CacheNodePtr tail;

    void move_to_tail(CacheNodePtr nodePtr);

public:
    CacheNodePtr get(string fileName);
    CacheNodePtr insert(string fileName, char* fileContent, size_t fileSize);
    void printAllFileNames();

    Cache(size_t size); //size in bytes
    Cache();
    //~Cache();
};

#endif
