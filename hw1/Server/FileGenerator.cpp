#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <cstring>

const size_t MB = 1024 * 1024;
const size_t KB = 1024;
const size_t BUFFER_SIZE = 1024;

using namespace std;

void genFileMB(string fileName, size_t size_in_MB){
    char buffer[BUFFER_SIZE];

    ofstream fou(fileName.c_str(), ios::out | ios::binary);
    memset(buffer, '1', BUFFER_SIZE);
    //printf("%s\n", buffer);
    size_t filledSize = 0;
    while (filledSize < size_in_MB * MB){
        fou.write(buffer, BUFFER_SIZE);
        filledSize += BUFFER_SIZE;
    }
    fou.close();
}

void genFileKB(string fileName, size_t size_in_KB){
    char buffer[BUFFER_SIZE];

    ofstream fou(fileName.c_str(), ios::out | ios::binary);
    memset(buffer, '1', BUFFER_SIZE);
    //printf("%s\n", buffer);
    size_t filledSize = 0;
    while (filledSize < size_in_KB * KB){
        fou.write(buffer, BUFFER_SIZE);
        filledSize += BUFFER_SIZE;
    }
    fou.close();
}

void genFileBytes(string fileName, size_t size_in_Bytes){
    char buffer[2];

    ofstream fou(fileName.c_str(), ios::out | ios::binary);
    memset(buffer, '1', 2);
    //printf("%s\n", buffer);
    size_t filledSize = 0;
    while (filledSize < size_in_Bytes){
        fou.write(buffer, 2);
        filledSize += 2;
    }
    fou.close();
}

int main(int argc, char *argv[]) {
    genFileMB("test1_32MB.txt", 32);
    genFileMB("test2_32MB.txt", 32);
    genFileMB("test3_10MB.txt", 10);
    gen

    genFileKB("test1_32KB.txt", 32);
    genFileKB("test2_32KB.txt", 32);
    genFileKB("test3_10KB.txt", 10);

    genFileBytes("test1_32Bytes.txt", 32);
    genFileBytes("test2_32Bytes.txt", 32);
    genFileBytes("test3_10Bytes.txt", 10);

    return 0;
}