#include "Cache.h"

#include <string>
#include <cstring>

using namespace std;

Cache::Cache() {
    m_capacity = m_remainCapacity = 0;
    head = new CacheNode("");
    tail = new CacheNode("");


    head->next = tail;
    tail->prev = head;

//    printf("%x, %x\n", (unsigned long long)(void*)head, (unsigned long long)(void*)tail);
}

Cache::Cache(size_t size) {
//    m_capacity = m_remainCapacity = 0;
//    head = new CacheNode("");
//    tail = new CacheNode("");
//
//
//    head->next = tail;
//    tail->prev = head;
//
//    printf("%x, %x\n", (unsigned long long)(void*)head, (unsigned long long)(void*)tail);
    *this = Cache();
    m_remainCapacity = m_capacity = size;
}

CacheNodePtr Cache::get(string filename) {
    unordered_map<string, CacheNodePtr>::const_iterator iter = m_map.find(filename);

    if (iter == m_map.end()) {
        //printf("%x, %x\n", (unsigned long long)(void*)head, (unsigned long long)(void*)tail);
        return NULL;
    }

    //printf("inside get iter->second = %x \n", (unsigned long long)(void*)iter->second);
    //remove node from current position
    iter->second->prev->next = iter->second->next;
    iter->second->next->prev = iter->second->prev;

    //move node to tail
    move_to_tail(iter->second);

    //printAllFileNames();

    return iter->second;
}

CacheNodePtr Cache::insert(string fileName, char* fileContent, size_t fileSize) {

    //printf("%x, %x\n", (unsigned long long)(void*)head, (unsigned long long)(void*)tail);
//    printf("DEBUG cache insert begin: %lu, %lu, %lu\n", m_capacity, m_remainCapacity, fileSize);

    if (fileSize > m_capacity) {
        return NULL;
    }

    while (fileSize > m_remainCapacity){
        //kick out least recently used cache items until
        // we have enough space for current file
//        printf("DEBUG cache insert 1\n");

        m_remainCapacity += head->next->m_size;

//        printf("DEBUG cache insert 2\n");

        m_map.erase(head->next->m_fileName);
        head->next = head->next->next;
        head->next->prev = head;

//        printf("DEBUG cache insert 3\n");
    }

    CacheNodePtr newFileNode = new CacheNode(fileName, fileContent, fileSize);

    //printf("++++++++++++\n");
    //m_map.emplace(fileName, newFileNode);
    m_map[fileName] = newFileNode;
    m_remainCapacity -= fileSize;
    //printf("-------------\n");
    move_to_tail(newFileNode);

    return newFileNode;
}

void Cache::move_to_tail(CacheNodePtr nodePtr) {
//    printf("%x\n", (unsigned long long)(void*)tail);

    nodePtr->prev = tail->prev;
    nodePtr->prev->next = nodePtr;
    nodePtr->next = tail;
    tail->prev = nodePtr;

//    puts("zui hou yi hang");
}

void Cache::printAllFileNames() {
    printf("--------- print all file names in cache ---------\n");
    for ( auto it = m_map.cbegin(); it != m_map.cend(); ++it ){
//        printf("filename = %s, address = %x, filecontent = %x, filesize = %lu, prev_name = %s, next_name = %s \n",
//                it->first.c_str(), (unsigned long long)(void *)it->second,
//                (unsigned long long)(void*)it->second->m_content, strlen(it->second->m_content),
//                it->second->prev->m_fileName.c_str(), it->second->next->m_fileName.c_str());
        printf("filename = %s, prev_name = %s, next_name = %s \n",
                it->first.c_str(),
                /*(unsigned long long)(void*)it->second->m_content, strlen(it->second->m_content),*/
                it->second->prev->m_fileName.c_str(), it->second->next->m_fileName.c_str());
    }
    printf("\n");
    printf("-------------------------------------------------\n");
}

//Cache::~Cache(){
//    CacheNodePtr p = head, next_p = NULL;
//    while (p != NULL) {
//        next_p = p->next;
//        p->~FileNode();
//        delete p;
//        p = next_p;
//    }
//}