#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <fstream>

using namespace std;

const size_t BUFFER_SIZE = 1024;
const int CONNECT_TIMEOUT = 3;

static int sTimeout = 0;

void error(const char *msg) {
    perror(msg);
    exit(1);
}

static void AlarmHandler(int sig) {
    sTimeout = 1;
}

int main(int argc, char *argv[]) {
    int sockfd, port, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[BUFFER_SIZE];

    /* check arguments */
    if (argc != 5) {
        fprintf(stderr, "Usage : %s server_host server_port file_name save_directory\n", argv[0]);
        exit(-1);
    }

    string filePath = string(argv[4]);
    /* check if the file path exist */
    struct stat sb;
    if(stat(argv[4], &sb) == -1) {
        error("stat error");
    }

    signal(SIGALRM, AlarmHandler);
    sTimeout = 0;

    /* create socket
     * the 2nd parameter is type of socket. SOCK_STREAM is TCP
     * the 3rd parameter is protocol. Set as 0 if the let the OS choose automatically
     * */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        error("ERROR opening socket");
    }

    /* resolve hostname */
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        error("ERROR, no such host");
    }
    /* set serv_addr */
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);

    port = atoi(argv[2]);
    if (port < 2000 || port > 65535) {
        error("ERROR on port number");
    }
    serv_addr.sin_port = htons(port);

    // set timeout for connecting
    alarm(CONNECT_TIMEOUT);

    /* connect to server */
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        if (sTimeout) {
            error("ERROR time out connecting the server");
        } else {
            error("ERROR connecting server");
        }
    };

    //printf("after connection\n");

    /* send file name to server */

    string fileName = string(argv[3]);
    bzero(buffer, BUFFER_SIZE);
    memcpy(buffer, fileName.c_str(), fileName.size());
    buffer[fileName.size()] = 0;
    n = write(sockfd, buffer, strlen(buffer));
    if (n < 0) {
        error("ERROR writing to socket");
    }

    //shutdown(sockfd, SHUT_WR);
    //printf("Send %s complete\n", fileName.c_str());

    /* receive state flag from server
    * 0: file does not exist
    * 1: file exist
    * */
    n = read(sockfd, buffer, 1);
    //printf("after read state flag %c\n", buffer[0]);

    if (buffer[0] == '0') {
        /* File does not exist on server */
        printf("File %s does not exist in the server\n", fileName.c_str());
    } else {
        /* receive file data from server */
        string fullFileName = filePath + "/" + fileName;
        //cout << fullFileName << endl;
        ofstream fileOutputStream;
        fileOutputStream.open(fullFileName.c_str(), ios::out | ios::binary);
        bzero(buffer, BUFFER_SIZE);
        n = read(sockfd, buffer, BUFFER_SIZE);
        if (n < 0) {
            error("ERROR reading from socket");
        }
        /* If n == 0, then EOF is reached */
        while (n != 0) {
            fileOutputStream.write(buffer, n);
            n = read(sockfd, buffer, BUFFER_SIZE);
            if (n < 0) {
                error("ERROR reading from socket");
            }
        }
        fileOutputStream.close();
        printf("File %s saved\n", fileName.c_str());
    }
    close(sockfd);

    return 0;
}